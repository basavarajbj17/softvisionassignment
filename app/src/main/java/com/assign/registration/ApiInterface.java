package com.assign.registration;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Basavaraj on 5/29/2017.
 */

public interface ApiInterface {

    @GET("getMasterData")
    Call<DisplayData> getMasterData();

    @POST("profile")
    Call<JsonObject> setUserInformation(@Body UserInfo userInfo);
}
